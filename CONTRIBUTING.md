You can propose your idea on the RedBricks platform by creating a new brick: 
https://redbricks.games/home/redbricks-107/bricks  
If you want to collaborate to the source code, you can submit your merge 
request.