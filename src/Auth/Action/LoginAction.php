<?php
namespace App\Auth\Action;

use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface;
use Framework\Api\FacebookConnect;

class LoginAction
{

    /**
     * @var RendererInterface
     */
    private $renderer;
    
    /**
     *
     * @var FacebookConnect
     */
    protected $facebook;

    public function __construct(RendererInterface $renderer, FacebookConnect $facebook)
    {
        $this->renderer = $renderer;
        $this->facebook = $facebook;
    }

    public function __invoke(ServerRequestInterface $request)
    {
		//var_dump($request);die();
                $url = $this->facebook->login("https://redbricks.games/login/facebook");
		return $this->renderer->render('@auth/login', compact('url'));
    }
}
