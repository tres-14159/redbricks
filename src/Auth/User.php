<?php

namespace App\Auth;

class User implements \Framework\Auth\User {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $role;

    /**
     *
     * @var string
     */
    protected $email;

    /**
     *
     * @var string
     */
    protected $password;

    /**
     *
     * @var string
     */
    protected $passwordReset;

    /**
     *
     * @var DateTime
     */
    protected $passwordResetAt;

    /**
     *
     * @var int
     */
    protected $active;

    /**
     *
     * @var string
     */
    protected $activationKey;

    /**
     *
     * @var string
     */
    protected $stripeUserId;

    /**
     *
     * @var string
     */
    protected $langcode;

    /**
     * @return null|string
     */
    function getStripeUserId(): ?string {
        return $this->stripeUserId;
    }

    /**
     * @param null|string $stripeUserId
     */
    function setStripeUserId(?string $stripeUserId) {
        $this->stripeUserId = $stripeUserId;
    }

    /**
     * @return string
     */
    public function getUsername(): string {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getRole(): string {
        return $this->role;
    }

    /**
     * @return string[]
     */
    public function getRoles(): array {
        return [];
    }

    /**
     * @return null|string
     */
    public function getPasswordReset(): ?string {
        return $this->passwordReset;
    }

    /**
     * @param null|string $passwordReset
     */
    public function setPasswordReset(?string $passwordReset) {
        $this->passwordReset = $passwordReset;
    }

    /**
     * 
     * @param \DateTime|null|string $date
     */
    public function setPasswordResetAt($date) {
        if (is_string($date)) {
            $this->passwordResetAt = new \DateTime($date);
        } else {
            $this->passwordResetAt = $date;
        }
    }

    /**
     * @return null|\DateTime
     */
    public function getPasswordResetAt(): ?\DateTime {
        return $this->passwordResetAt;
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string {
        return $this->email;
    }

    /**
     * @param null|string $email
     */
    public function setEmail(?string $email) {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): string {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password) {
        $this->password = $password;
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @return bool
     */
    public function isActive(): bool {
        return $this->active == 1;
    }

    /**
     * 
     * @param int $active
     */
    public function setActive(int $active) {
        $this->active = $active;
    }

    /**
     * 
     * @return string|null
     */
    public function getActivationKey(): ?string {
        return $this->activationKey;
    }

    /**
     * 
     * @return int
     */
    function getActive(): int {
        return $this->active;
    }

    /**
     * 
     * @param string $username
     */
    function setUsername(string $username) {
        $this->username = $username;
    }

    /**
     * 
     * @param string $role
     */
    function setRole(string $role) {
        $this->role = $role;
    }

    /**
     * 
     * @param string|null $activationKey
     */
    function setActivationKey(?string $activationKey) {
        $this->activationKey = $activationKey;
    }

    /**
     * 
     * @return string
     */
    function getLangcode(): string {
        return $this->langcode;
    }

    /**
     * 
     * @param string $langcode
     */
    function setLangcode(string $langcode) {
        $this->langcode = $langcode;        
    }
    
    function setLangInterface() {
        if($this->langcode == "fr"){
            $lang = "fr_FR";
        } elseif ($this->langcode == "es") {
            $lang = "es_ES";
        } else {
            $lang = "en_US";
        }
        putenv("LC_ALL=$lang");
        setlocale(LC_ALL, $lang);
    }
    
    static function setLang(string $langcode) {
        if($langcode == "fr"){
            $lang = "fr_FR";
        } elseif ($langcode == "es") {
            $lang = "es_ES";
        } else {
            $lang = "en_US";
        }
        putenv("LC_ALL=$lang");
        setlocale(LC_ALL, $lang);
        putenv("LANG=$langcode");
    }

}
