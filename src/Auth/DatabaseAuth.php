<?php

namespace App\Auth;

use Framework\Auth;
use Framework\Auth\User;
use Framework\Database\NoRecordException;
use Framework\Session\SessionInterface;

class DatabaseAuth implements Auth {

    /**
     * @var UserTable
     */
    private $userTable;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var \App\Auth\User
     */
    private $user;

    public function __construct(UserTable $userTable, SessionInterface $session) {
        $this->userTable = $userTable;
        $this->session = $session;
    }

    public function login(string $username, string $password, ?bool $remember = false): ?User {
        if (empty($username) || empty($password)) {
            return null;
        }

        /** @var \App\Auth\User $user */
        try {
            $user = $this->userTable->findBy('username', $username);
        } catch (NoRecordException $exception) {
            try {
                $user = $this->userTable->findBy('email', $username);
            } catch (NoRecordException $exception) {
                return null;
            }
        }
        if ($user && password_verify($password, $user->getPassword()) && $user->isActive()) {
            $this->session->set('auth.user', $user->getId());
            $this->session->set('auth.role', $user->getRole());
            if ($remember != null) {
                setcookie('auth', $user->getId() . '--' . sha1($username . $user->getPassword() . $_SERVER['REMOTE_ADDR']), time() + 3600 * 24 * 3, '/', 'redbricks.games', true, true);
            }
            return $user;
        }

        return null;
    }

    public function logout(): void {
        $this->session->delete('auth.user');
        $this->session->delete('auth.role');
        setcookie('auth', '', time() - 3600, '/', 'redbricks.games', true, true);
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User {
        if ($this->user) {
            return $this->user;
        }
        $userId = $this->session->get('auth.user');
        if ($userId) {
            try {
                $this->user = $this->userTable->find($userId);
                return $this->user;
            } catch (NoRecordException $exception) {
                $this->session->delete('auth.user');
                return null;
            }
        } else if (isset($_COOKIE['auth'])) {
            $auth = explode('--', $_COOKIE['auth']);
            //echo '<pre>'; var_dump($auth); echo '</pre>';
            $userId = $auth[0];
            //echo '<pre>'; var_dump($userId); echo '</pre>';
            try {
                $user = $this->userTable->find($userId);
                //echo '<pre>'; var_dump($user); echo '</pre>';
                $key = sha1($user->getUsername() . $user->getPassword() . $_SERVER['REMOTE_ADDR']);
                //echo '<pre>'; var_dump($key, $user->getPassword()); echo '</pre>'; die();
                if ($key == $auth[1]) {
                    $this->setUser($user);
                    setcookie('auth', $user->getId() . '--' . $key, time() + 3600 * 24 * 3, '/', 'redbricks.games', true, true);
                    return $this->user;
                } else {
                    setcookie('auth', '', time() - 3600, '/', 'redbricks.games', true, true);
                    return null;
                }
            } catch (NoRecordException $exception) {
                $this->session->delete('auth.user');
                setcookie('auth', '', time() - 3600, '/', 'redbricks.games', true, true);
                return null;
            }
        }
        return null;
    }

    public function setUser(\App\Auth\User $user): void {
        $this->session->set('auth.user', $user->getId());
        $this->session->set('auth.role', $user->getRole());
        $this->user = $user;
    }

}
