<?php
namespace App\Start;

use Framework\Module;
use Framework\Renderer\RendererInterface;
use Framework\Router;

class StartModule extends Module
{
    const DEFINITIONS = __DIR__ . '/definitions.php';
    
    public function __construct(Router $router, RendererInterface $renderer)
    {
        $renderer->addPath('start', __DIR__);
		$router->get('/start', StartAction::class, 'start');
    }
}
