<?php

namespace App\Discussion\Table;

use App\Auth\UserTable;
use Framework\Database\Table;
use App\Discussion\Entity\Message;
use Framework\Database\Query;

class MessageTable extends Table {

    protected $entity = Message::class;
    protected $table = "messages";

    /**
     * 
     * @param int $id
     * @return Query
     */
    public function findAllThread(int $id): Query {
        $user = new UserTable($this->pdo);
        return $this->makeQuery()
                        ->select("m.*, u.username, u.displayname, u.avatar")
                        ->join($user->getTable() . " as u", "u.id=m.id_from")
                        ->where("m.id_thread = $id")
                        ->order("m.created_at");
    }

}
