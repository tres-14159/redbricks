<?php

namespace App\Discussion\Entity;

class Thread {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $idUsers;

    /**
     *
     * @var int
     */
    protected $lastMessage;

    /**
     * 
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return string
     */
    public function getIdUsers(): string {
        return $this->idUsers;
    }

    /**
     * 
     * @return int
     */
    public function getLastMessage(): int {
        return $this->lastMessage;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param string $idUsers
     */
    function setIdUsers(string $idUsers) {
        $this->idUsers = $idUsers;
    }

    /**
     * 
     * @param int $lastMessage
     */
    function setLastMessage(int $lastMessage) {
        $this->lastMessage = $lastMessage;
    }

}
