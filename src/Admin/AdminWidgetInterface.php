<?php
namespace App\Admin;

interface AdminWidgetInterface
{
    
//  public function getPosition(): int;//A développer si on veut régler la position des widgets
    
    public function render(): string;

    public function renderMenu(): string;
}
