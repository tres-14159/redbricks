<?php

namespace App\Blog\Table;

use Framework\Database\Table;
use App\Blog\Entity\FeatureContent;
use App\Blog\Table\FeatureTable;
use App\Localization\Table\LangTable;
use Framework\Database\Query;

class FeatureContentTable extends Table {

    protected $entity = FeatureContent::class;
    protected $table = "features_content";

    public function findAll(): Query {
        $lang = new LangTable($this->pdo);
        return $this->makeQuery()
                        ->select("f.*, l.name as language")
                        ->join($lang->getTable() . " as l", "l.code = f.langcode");
    }

    public function findAllByFeature(int $featureId): Query {
        return $this->findAll()
                        ->where("f.feature_id = $featureId")
                        ->order("f.langcode");
    }

    public function findAllByGame(int $gameId): Query {
        $feature = new FeatureTable($this->pdo);
        return $this->findAll()
                        ->join($feature->getTable() . " as feature", "feature.id = f.feature_id")
                        ->where("feature.project_id = $gameId")
                        ->order("f.feature_id DESC, f.langcode");
    }

    public function findAllByGameWithUnusedLanguage(int $gameId): Query {
        $feature = new FeatureTable($this->pdo);
        return $this->findAll()
                        ->select("f.feature_id, COUNT(f.feature_id) as nbLang")
                        ->join($feature->getTable() . " as feature", "feature.id = f.feature_id")
                        ->where("feature.project_id = $gameId")
                        ->order("f.id")
                        ->groupBy("f.feature_id");
    }

    public function find(int $id) {
        return $this->findAll()
                        ->where("f.id = $id")
                        ->fetchOrFail();
    }

    public function updateByFeature(int $id, array $params): bool {
        $fieldQuery = $this->buildFieldQuery($params);
        $params["feature_id"] = $id;
        $query = $this->pdo->prepare("UPDATE {$this->table} SET $fieldQuery WHERE feature_id= :feature_id AND langcode = 'en'");
        return $query->execute($params);
    }

}
