<?php

namespace App\Blog\Table;

use Framework\Database\Table;
use Framework\Database\Query;

class StatusTable extends Table {

    protected $table = "status";

    public function findAll(): Query {
        $name = 's.name_' . getenv("LANG");
        return $this->makeQuery()
                        ->select("s.id, $name as name, s.slug");
    }

    public function findList(): array {
        $name = 'name_' . getenv("LANG");
        $results = $this->pdo
                ->query("SELECT id, $name as name FROM {$this->table}")
                ->fetchAll(\PDO::FETCH_NUM);
        $list = [];
        foreach ($results as $result) {
            $list[$result[0]] = $result[1];
        }
        return $list;
    }

}
