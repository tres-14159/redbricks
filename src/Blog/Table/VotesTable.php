<?php

namespace App\Blog\Table;

use Framework\Database\Table;

class VotesTable extends Table {

    protected $table = "votes";

    public function findBy(string $field, string $value) {
        return $this->makeQuery()->where("$field = :field")->params(["field" => $value]);
    }

    public function findVote($userId, $featureId) {
        return $this->makeQuery()->where("user_id = :userId AND feature_id= :featureId")->params(["userId" => $userId, "featureId" => $featureId]);
    }

    public function getAllVote($gameId) {
        $feature = new FeatureTable($this->pdo);
        return $this->makeQuery()
                        ->select("v.*")
                        ->join($feature->getTable() . " as f", "f.id=v.feature_id")
                        ->where("f.project_id = $gameId");
    }

}
