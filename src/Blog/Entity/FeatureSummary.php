<?php

namespace App\Blog\Entity;

class FeatureSummary {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var int
     */
    protected $gameId;

    /**
     *
     * @var string|null
     */
    protected $description;

    /**
     *
     * @var string
     */
    protected $langcode;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return int
     */
    function getGameId(): int {
        return $this->gameId;
    }

    /**
     * 
     * @return string|null
     */
    function getDescription(): ?string {
        return $this->description;
    }

    /**
     * 
     * @return string
     */
    function getLangcode(): string {
        return $this->langcode;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param int $gameId
     */
    function setGameId(int $gameId) {
        $this->gameId = $gameId;
    }

    /**
     * 
     * @param string|null $description
     */
    function setDescription(?string $description) {
        $this->description = $description;
    }

    /**
     * 
     * @param string $langcode
     */
    function setLangcode(string $langcode) {
        $this->langcode = $langcode;
    }

}
