<?php
namespace App\Blog;

use Framework\Auth;
use App\User\UserWidgetInterface;
use App\Blog\Table\LinkGamePlatformTable;
use App\Blog\Table\GameTable;
use Framework\Renderer\RendererInterface;

class UserBlogWidget implements UserWidgetInterface
{

    /**
     * @var RendererInterface
     */
    private $renderer;
    
    /**
     * @var GameTable
     */
    private $gameTable;

    /**
    * @var LinkGamePlatformTable
    */
    private $LinkGamePlatformTable;
    
    /**
    * @var Auth
    */
    private $Auth;
        
     
    public function __construct(RendererInterface $renderer, GameTable $gameTable, LinkGamePlatformTable $LinkGamePlatformTable, Auth $auth)
    {
        $this->renderer= $renderer;
        $this->gameTable = $gameTable;
        $this->linkGamePlatformTable = $LinkGamePlatformTable;
        $this->auth = $auth;
    }
    
    public function render(): string
    {
        $user= $this->auth->getUser();

        $count= $this
                    ->gameTable
                    ->findByAndCount("user_id", $user->getId());
        return $this->renderer->render("@blog/user/widget", compact("count"));
    }

    public function renderMenu(): string
    {
        return $this->renderer->render("@blog/user/menu");
    }
}
