<?php

namespace App\Blog\Actions;

use Framework\Response\RedirectResponse;
use Framework\Session\PHPSession;
use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface;

class SearchAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(
    RendererInterface $renderer, PHPSession $session
    ) {
        $this->renderer = $renderer;
        $this->session = $session;
    }

    public function __invoke(ServerRequestInterface $request) {
        $uri = "/home/search/";

        $params = $request->getParsedBody();
//var_dump($params);die();
        if (isset($params["inputSearch"]) and $params["inputSearch"] !== "" and isset($params["search"])) {
            return new RedirectResponse($uri . $params["inputSearch"]);
        } else {
            return (new \GuzzleHttp\Psr7\Response())->withHeader('Location', substr($uri, 0, -7));
        }
    }

}
