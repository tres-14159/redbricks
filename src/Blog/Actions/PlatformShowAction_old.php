<?php

namespace App\Blog\Actions;

use Framework\Session\PHPSession;
use App\Blog\Table\LinkGamePlatformTable;
use App\Blog\Table\PlatformTable;
use App\Blog\Table\LicenceTable;
use App\Blog\Table\GameTable;
use Framework\Actions\RouterAwareAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class PlatformShowAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var GameTable
     */
    private $gameTable;

    /**
     * @var LicenceTable
     */
    private $licenceTable;

    /**
     * @var PlatformTable
     */
    private $platformTable;

    /**
     * @var LinkGamePlatformTable
     */
    private $linkGamePlatformTable;

    /**
     * @var SessionInterface
     */
    private $session;
    private $platforms_filter;

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer,
            GameTable $gameTable,
            LicenceTable $licenceTable,
            LinkGamePlatformTable $linkGamePlatformTable,
            PlatformTable $platformTable,
            PHPSession $session
    ) {
        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->licenceTable = $licenceTable;
        $this->linkGamePlatformTable = $linkGamePlatformTable;
        $this->platformTable = $platformTable;
        $this->session = $session;
    }

    public function __invoke(Request $request) {
        $params = $request->getQueryParams();

        //On entoure les éléments du filtre de guillemets
        $platforms_filter = $this->session->get("platforms");

        if (isset($platforms_filter)) {
            $platforms_filter = join(",", array_map(function ($value) {
                        return "'" . $value . "'";
                    }, $platforms_filter));
            //var_dump($this->gameTable->findAllLinksPlatform()-> where ('p.slug IN (' . $platforms_filter . ')')->fetchAll());die();
            $LinkGamePlatformTable = $this->gameTable->findAllLinksPlatform()->where('p.slug IN (' . $platforms_filter . ')')->fetchColumn();
        } else {
            $LinkGamePlatformTable = $this->gameTable->findAllLinksPlatform()->fetchColumn();
        }

        //var_dump($LinkGamePlatformTable);die();
        //findPaginated(12): pagination réglée à 12
        $games = $this->gameTable->findPublicForPlatform($LinkGamePlatformTable)->paginate(12, $params["p"] ?? 1);
        //var_dump($games);die();
        $licences = $this->licenceTable->findAll();
        $platforms = $this->platformTable->findAll();
        $page = $params["p"] ?? 1;

        return $this->renderer->render("@blog/index", compact("games", "licences", "licence", "platforms", "platform", "platforms_filter", "page"));
    }

}
