<?php

namespace App\Blog\Actions;

use App\Blog\Table\VotesTable;
use Framework\Actions\RouterAwareAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Session\FlashService;
use Psr\Http\Message\ServerRequestInterface;
use App\Blog\Table\GameTable;
use App\Blog\Table\FeatureTable;

class GameFeatureVoteAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var FlashService
     */
    private $flashService;

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var FeatureTable
     */
    protected $featureTable;

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer, Router $router, VotesTable $votesTable, FlashService $flashService, GameTable $gameTable, FeatureTable $featureTable
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->votesTable = $votesTable;
        $this->flashService = $flashService;
        $this->gameTable = $gameTable;
        $this->featureTable = $featureTable;
    }

    public function __invoke(ServerRequestInterface $request) {
        if ($request->getMethod() == "POST") {
            $params = $request->getParsedBody();
            $vote = (isset($params['voteYes'])) ? 1 : 0;
            $userId = $params['userId'];
            $featureId = $params['featureId'];
            $featureSlug = $params['featureSlug'];
            //echo '<pre>'; var_dump($params, $userId, $featureId, $vote); echo '</pre>'; die();
            $this->votesTable->insert([
                'user_id' => $userId,
                'feature_id' => $featureId,
                'vote' => $vote
            ]);
            $this->flashService->success(_('Thank you for your vote.'));
            //var_dump($vote,$userId,$featureId,$featureSlug);die();
            if ($params['path_back'] == "bricks") {
                $feature = $this->featureTable->find($featureId);
                $game = $this->gameTable->find($feature->getProjectId());
                return $this->redirect("blog.show.bricks", [
                            "slug" => $game->getSlug(),
                            "id" => $game->getId()
                ]);
            }
            return $this->redirect("blog.feature.showOneFeature", [
                        "slug" => $featureSlug,
                        "id" => $featureId
            ]);
        }
        $slug = $request->getAttribute("slug");
        $id = $request->getAttribute("id");
        return $this->redirect("blog.feature.showOneFeature", [
                    "slug" => $slug,
                    "id" => $id
        ]);
    }

}
