<?php

namespace App\Blog\Actions;

use Framework\Session\PHPSession;
use App\Blog\Table\PlatformTable;
use App\Blog\Table\FeatureTable;
use App\Blog\Table\LicenceTable;
use App\Blog\Table\GameTable;
use App\Shop\Table\SubscriptionTable;
use App\Blog\Table\FavoriteTable;
use Framework\Actions\RouterAwareAction;
use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Framework\Auth;

class GameIndexAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var GameTable
     */
    private $gameTable;

    /**
     * @var FeatureTable
     */
    private $featureTable;

    /**
     * @var LicenceTable
     */
    private $licenceTable;

    /**
     * @var PlatformTable
     */
    private $platformTable;

    /**
     *
     * @var FavoriteTable
     */
    protected $favoriteTable;
    
    /**
     *
     * @var SubscriptionTable
     */
    protected $subscriptionTable;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     *
     * @var Auth
     */
    protected $auth;
    protected $routePrefix = "blog.index";

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer, GameTable $gameTable, FeatureTable $featureTable, LicenceTable $licenceTable, PlatformTable $platformTable, FavoriteTable $favoriteTable, SubscriptionTable $subscriptionTable, PHPSession $session, Auth $auth
    ) {
        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->featureTable = $featureTable;
        $this->licenceTable = $licenceTable;
        $this->platformTable = $platformTable;
        $this->subscriptionTable = $subscriptionTable;
        $this->favoriteTable = $favoriteTable;
        $this->session = $session;
        $this->auth = $auth;
    }

    public function __invoke(Request $request) {
        $this->session->delete('routeReturn');
        $user = $this->auth->getUser();
        if ($request->getMethod() === "POST" && $user != null) {
            $params = $request->getParsedBody();
            $this->favoriteTable->change($params['gameId'], $user->getId());
        }

        if (substr((string) $request->getUri(), -6) === "random") {
            return $this->random($request);
        } else if (substr((string) $request->getUri(), -3) === "new") {
            return $this->index($request);
        } else if (substr((string) $request->getUri(), -9) === "favorites" && $user != null) {
            return $this->favorites($request);
        }
        return $this->updated($request);
    }

    private function index(Request $request): string {
        $params = $request->getQueryParams();
        $this->routePrefix .= ".new";
        $this->renderer->addGlobal("routePrefix", $this->routePrefix);
        $games = $this->gameTable->findPublic()->paginate(12, $params["p"] ?? 1);
        $page = $params["p"] ?? 1;
        return $this->renderer->render("@blog/index", $this->formParams(compact("games", "page")));
    }

    private function updated(Request $request): string {
        $params = $request->getQueryParams();
        $this->renderer->addGlobal("routePrefix", $this->routePrefix);
        $games = $this->gameTable->findPublicOrderByUpdated()->paginate(12, $params["p"] ?? 1);
        $page = $params["p"] ?? 1;
        return $this->renderer->render("@blog/index", $this->formParams(compact("games", "page")));
    }

    private function random(Request $request): string {
        $params = $request->getQueryParams();
        $this->routePrefix .= ".random";
        $this->renderer->addGlobal("routePrefix", $this->routePrefix);
        $games = $this->gameTable->findPublicOrderByRand()->paginate(12, $params["p"] ?? 1);
        $page = $params["p"] ?? 1;
        return $this->renderer->render("@blog/index", $this->formParams(compact("games", "page")));
    }

    private function favorites(Request $request): string {
        $params = $request->getQueryParams();
        $user = $this->auth->getUser();
        $this->routePrefix .= ".favorites";
        $this->renderer->addGlobal("routePrefix", $this->routePrefix);
        $games = $this->gameTable->findFavorite($user->getId())->paginate(12, $params["p"] ?? 1);
        $page = $params["p"] ?? 1;
        return $this->renderer->render("@blog/index", $this->formParams(compact("games", "page")));
    }

    /**
     * Permet de traiter les paramètres à envoyer à la vue
     *
     * @param $params
     * @return array
     */
    protected function formParams(array $params): array {
        $params['licences'] = $this->licenceTable->findAll();
        $params['platforms'] = $this->platformTable->findAll();
        $params['liens_platforms'] = $this->gameTable->findAllPlatform();
        $params['contributorsNb'] = $this->subscriptionTable->findAllContributors();
        $params['features'] = $this->featureTable->findAll();
        $params['lien'] = "null";
        $params['inputSearch'] = "null";
        //echo '<pre>';        var_dump($params); echo '</pre>'; die();
        if ($this->auth->getUser() != null) {
            $user = $this->auth->getUser();
            $params['favorites'] = $this->favoriteTable->findAllForUser($user->getId());
        }
        return $params;
    }

}
