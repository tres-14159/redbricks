<?php

namespace App\Blog\Actions;

use Framework\Actions\CrudAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use App\Blog\Table\FeatureSummaryTable;
use App\Blog\Table\GameTable;
use App\Localization\Table\LangTable;
use Framework\Session\FlashService;
use Framework\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface;

class SummaryCrudAction extends CrudAction {

    /**
     *
     * @var RendererInterface
     */
    protected $renderer;

    /**
     *
     * @var Router
     */
    protected $router;

    /**
     *
     * @var FeatureSummaryTable
     */
    protected $table;

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var LangTable
     */
    protected $langTable;

    /**
     *
     * @var FlashService
     */
    protected $flash;

    /**
     *
     * @var SessionInterface
     */
    protected $session;

    public function __construct(
            RendererInterface $renderer, Router $router, FeatureSummaryTable $table, GameTable $gameTable, LangTable $langTable, FlashService $flash, SessionInterface $session
    ) {
        parent::__construct($renderer, $router, $table, $flash);
        $this->gameTable = $gameTable;
        $this->langTable = $langTable;
        $this->session = $session;
    }

    public function create(ServerRequestInterface $request) {
        $projectId = $request->getAttribute("projectId");
        $game = $this->gameTable->find($projectId);
        $this->validateAccessRights($game->getUserId(), $game->getId());
        if ($request->getMethod() === "POST") {
            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                $params = $request->getParsedBody();
                $this->table->insert([
                    "game_id" => $projectId,
                    "description" => $params['description'],
                    "langcode" => $params['langcode']
                ]);
                $this->flash->success(_("Summary updated"));
                if ($params['langcode'] != 'en') {
                    return $this->index($request);
                }
            } else {
                $this->flash->error(_("Error : The summary must be longer."));
            }
        }
        return $this->redirect('blog.user.features', ['id' => $projectId]);
    }

    public function edit(ServerRequestInterface $request) {
        $projectId = $request->getAttribute("projectId");
        $summaryId = $request->getAttribute("id");
        if ($summaryId == 0) {
            return $this->create($request);
        }
        $game = $this->gameTable->find($projectId);
        $this->validateAccessRights($game->getUserId(), $game->getId());
        if ($request->getMethod() === "POST") {
            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                $params = $request->getParsedBody();
                $this->table->update($summaryId, [
                    "description" => $params['description']
                ]);
                $this->flash->success(_("Summary updated"));
                if ($params['langcode'] != 'en') {
                    return $this->index($request);
                }
            } else {
                $this->flash->error(_("Error : The summary must be longer."));
            }
        }
        return $this->redirect('blog.user.features', ['id' => $projectId]);
    }

    public function index(ServerRequestInterface $request): string {
        $projectId = $request->getAttribute("projectId");
        $games = $this->gameTable->find($projectId);
        $languages = $this->langTable->findAll();
        $summaries = $this->table->findAllLang($projectId);

        return $this->renderer->render("@blog/user/summary/index", compact("games", "languages", "summaries"));
    }

    protected function getValidator(ServerRequestInterface $request) {
        return parent::getValidator($request)
                        ->required("description")
                        ->length("description", 10);
    }

    protected function validateAccessRights(int $gameUserId, int $gameId) {
        $role = $this->session->get('auth.role');
        $userId = $this->session->get('auth.user');
        if ($role == null ||
                ($role == 'user' && $userId != $gameUserId && !$this->contributorTable->isContributor($gameId, $userId))) {
            throw new Auth\ForbiddenException();
        }
    }

}
