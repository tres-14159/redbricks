<?php

namespace App\News;

use Framework\Module;
use Framework\Router;
use Framework\Renderer\RendererInterface;
use Framework\Auth\LoggedInMiddleware;
use App\News\Actions\NewsCrudAction;
use App\News\Actions\NewsIndexAction;
use App\News\Actions\NewsShowAction;
use App\News\Actions\NewsListAction;

class NewsModule extends Module {

    public function __construct(Router $router, RendererInterface $renderer) {
        $renderer->addPath('news', __DIR__ . "/views");
        $router->get('/news/index/{id:[0-9]+}', [LoggedInMiddleware::class, NewsIndexAction::class], 'news');
        $router->crud("/news/{projectId:[0-9]+}", [LoggedInMiddleware::class, NewsCrudAction::class], "news");
        $router->get('/news/news/{id:[0-9]+}', NewsShowAction::class, "news.show");
        $router->get("/home/{slug:[a-z\-0-9]+}-{id:[0-9]+}/news", NewsListAction::class, "news.list");
    }

}
