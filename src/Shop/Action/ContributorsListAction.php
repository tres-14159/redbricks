<?php

namespace App\Shop\Action;

use Framework\Renderer\RendererInterface;
use Framework\Router;
use App\Blog\Table\GameTable;
use App\News\Table\NewsTable;
use App\Blog\Table\FeatureTable;
use App\Comments\Table\CommentsTable;
use App\Bug\Table\BugTable;
use App\Survey\Table\SurveyTable;
use Psr\Http\Message\ServerRequestInterface;
use App\Shop\Table\PendingPurchaseTable;
use App\Shop\Table\PurchaseTable;
use App\Auth\UserTable;

class ContributorsListAction {

    /**
     * @var RendererInterface
     */
    protected $renderer;

    /**
     *
     * @var Router
     */
    protected $router;

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var NewsTable
     */
    protected $newsTable;

    /**
     *
     * @var BugTable
     */
    protected $bugTable;

    /**
     *
     * @var CommentsTable
     */
    protected $commentsTable;

    /**
     *
     * @var FeatureTable
     */
    protected $featureTable;

    /**
     *
     * @var SurveyTable
     */
    protected $surveyTable;

    /**
     *
     * @var PendingPurchaseTable
     */
    protected $pendingPurchaseTable;

    /**
     *
     * @var PurchaseTable
     */
    protected $purchaseTable;
    
    /**
     *
     * @var UserTable
     */
    protected $userTable;

    public function __construct(
            RendererInterface $renderer, Router $router, GameTable $gameTable, NewsTable $newsTable, FeatureTable $featureTable, CommentsTable $commentsTable, BugTable $bugTable, SurveyTable $surveyTable, PendingPurchaseTable $pendingPurchaseTable, PurchaseTable $purchaseTable, UserTable $userTable
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->gameTable = $gameTable;
        $this->newsTable = $newsTable;
        $this->bugTable = $bugTable;
        $this->commentsTable = $commentsTable;
        $this->featureTable = $featureTable;
        $this->surveyTable = $surveyTable;
        $this->pendingPurchaseTable = $pendingPurchaseTable;
        $this->purchaseTable = $purchaseTable;
        $this->userTable = $userTable;
    }

    public function __invoke(ServerRequestInterface $request) {
        $slug = $request->getAttribute("slug");
        $gameId = $request->getAttribute("id");
        $game = $this->gameTable->findShow($gameId);

        $nbFeatures = $this->featureTable->findShow($gameId)->where("s.id>1")->count("*");
        $nbBugs = $this->bugTable->findShow($gameId)->count("*");
        $nbComments = $this->commentsTable->findCount($gameId);
        $nbNews = $this->newsTable->findShow($gameId)->count("*");
        $nbSurveys = $this->surveyTable->findShow($gameId)->count("*");

        $contributors = $this->userTable->contributorsForProject($gameId);
        //$contributors = $this->purchaseTable->contributorsForProject($gameId);
        //$contributorsPending = $this->pendingPurchaseTable->contributorsForProject($gameId);
        //$contributorsList = array_merge($contributors->fetchAll()->records, $contributorsPending->fetchAll()->records);
        //echo '<pre>';        var_dump($contributorsList, $contributorsPending->fetchAll()->records, $contributors->fetchAll()->records); echo '</pre>'; die();

        return $this->renderer->render('@shop/contributors', [
                    "game" => $game,
                    "nbFeatures" => $nbFeatures,
                    "nbBugs" => $nbBugs,
                    "nbComments" => $nbComments,
                    "nbNews" => $nbNews,
                    "nbSurveys" => $nbSurveys,
                    "contributors" => $contributors
        ]);
    }

}
