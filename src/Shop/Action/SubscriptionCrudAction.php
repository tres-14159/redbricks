<?php

namespace App\Shop\Action;

use Framework\Actions\CrudAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use App\Shop\Table\SubscriptionTable;
use App\Shop\Table\RewardTable;
use Framework\Session\FlashService;
use Psr\Http\Message\ServerRequestInterface;
use Framework\Api\Stripe;

class SubscriptionCrudAction extends CrudAction {
    //protected $viewPath = "@blog/user/features/feature";
    //protected $routePrefix = "blog.user.features";

    /**
     *
     * @var RendererInterface
     */
    protected $renderer;

    /**
     *
     * @var Router
     */
    protected $router;

    /**
     *
     * @var SubscriptionTable
     */
    protected $table;

    /**
     *
     * @var RewardTable
     */
    protected $rewardTable;

    /**
     *
     * @var FlashService
     */
    protected $flash;

    /**
     *
     * @var Stripe
     */
    protected $stripe;

    public function __construct(
            RendererInterface $renderer, Router $router, SubscriptionTable $table, RewardTable $rewardTable, FlashService $flash, Stripe $stripe
    ) {
        parent::__construct($renderer, $router, $table, $flash);
        $this->stripe = $stripe;
        $this->rewardTable = $rewardTable;
    }

    public function delete(ServerRequestInterface $request) {
        $subscriptionId = $request->getAttribute('id');
        $subscription = $this->table->find($subscriptionId);
        $this->stripe->cancelSubscription($subscription->getSubscriptionId(), array("stripe_account" => $subscription->getStripeAccount()));
        try {
            $reward = $this->rewardTable->find($subscription->getRewardId());
            $this->rewardTable->update($reward->getId(), [
                "contributors_nb" => $reward->getContributorsNb() - 1
            ]);
        } catch (Framework\Database\NoRecordException $e) {
            
        }
        $this->table->delete($subscriptionId);
        $this->flash->success(_("Subscription canceled"));

        return $this->redirect('shop.purchases.subscriptions');
    }

    public function create(ServerRequestInterface $request) {
        return $this->redirect('shop.purchases.subscriptions');
    }

    public function edit(ServerRequestInterface $request) {
        $this->redirect('shop.purchases.subscriptions');
    }

}
