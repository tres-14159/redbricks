<?php

namespace App\Shop\Table;

use Framework\Database\Table;
use App\Shop\Entity\Subscription;
use Framework\Database\Query;
use App\Blog\Table\GameTable;
use App\Shop\Table\PendingPurchaseTable;
use App\Shop\Table\PurchaseTable;
use App\Auth\UserTable;
use App\Shop\Table\RewardTable;

class SubscriptionTable extends Table {

    protected $entity = Subscription::class;
    protected $table = "subscriptions";

    public function findAllByUser(int $userId): Query {
        $game = new GameTable($this->pdo);
        return $this->makeQuery()
                        ->select("s.*, g.name as gameName")
                        ->join($game->getTable() . " as g", "g.id = s.game_id")
                        ->where("s.user_id = $userId")
                        ->order("s.created_at DESC");
    }

    public function findAllByGame(): Query {
        return $this->makeQuery()
                        ->select('SUM(amount)/100 as amount, s.game_id')
                        ->groupBy('game_id');
    }

    public function getMoneySubscripted(string $field, int $id) {
        return $this->makeQuery()
                        ->select('SUM(amount)/100')
                        ->where("$field = $id")
                        ->fetchColumnNum();
    }

    public function countSubscribers(string $field, int $id) {
        return $this->makeQuery()
                        ->select("count(DISTINCT user_id)")
                        ->where("$field = $id")
                        ->fetchColumnNum();
    }

    public function findAllContributors(): array {
        $pendingPurchases = new PendingPurchaseTable($this->pdo);
        $purchases = new PurchaseTable($this->pdo);
        $query = $this->pdo->prepare("SELECT derived.game_id AS gameId, COUNT(DISTINCT derived.user_id) AS nb FROM(
            SELECT DISTINCT user_id, game_id FROM {$pendingPurchases->getTable()}
            UNION SELECT DISTINCT user_id, game_id FROM {$purchases->getTable()}
            UNION SELECT DISTINCT user_id, game_id FROM {$this->table}
            ) AS derived
            GROUP BY derived.game_id");
        $query->execute();
        return $query->fetchAll();
    }

    public function findAllBy(string $field, string $value): Query {
        $user = new UserTable($this->pdo);
        $reward = new RewardTable($this->pdo);
        return $this->makeQuery()
                        ->select("s.*, u.username, r.title")
                        ->join($user->getTable() . " as u", "u.id = s.user_id")
                        ->join($reward->getTable() . " as r", "r.id = s.reward_id")
                        ->where("$field = $value")
                        ->order("s.reward_id, s.created_at");
    }

    public function findByUserGame(int $userId, int $gameId) {
        return $this->makeQuery()
                        ->where("user_id = $userId AND game_id = $gameId")
                        ->fetchOrFail();
    }

}
