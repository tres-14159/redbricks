<?php

namespace App\Shop\Entity;

class Reward {
    
    /**
     *
     * @var int
     */
    protected $id;
    
    /**
     *
     * @var int
     */
    protected $projectId;
    
    /**
     *
     * @var string
     */
    protected $title;
    
    /**
     *
     * @var string
     */
    protected $description;
    
    /**
     *
     * @var int
     */
    protected $amount;
    
    /**
     *
     * @var int
     */
    protected $contributorsMax;
    
    /**
     *
     * @var int
     */
    protected $contributorsNb;
    
    /**
     *
     * @var string 
     */
    protected $productId;
    
    /**
     *
     * @var string
     */
    protected $planId;
    
    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return int
     */
    function getProjectId(): int {
        return $this->projectId;
    }

    /**
     * 
     * @return string|null
     */
    function getTitle(): ?string {
        return $this->title;
    }

    /**
     * 
     * @return string|null
     */
    function getDescription(): ?string {
        return $this->description;
    }

    /**
     * 
     * @return int|null
     */
    function getAmount(): ?int {
        return $this->amount;
    }

    /**
     * 
     * @return int|null
     */
    function getContributorsMax(): ?int {
        return $this->contributorsMax;
    }

    /**
     * 
     * @return int
     */
    function getContributorsNb(): int {
        return $this->contributorsNb;
    }
    
    /**
     * 
     * @return string|null
     */
    function getProductId(): ?string {
        return $this->productId;
    }
    
    /**
     * 
     * @return string
     */
    function getPlanId(): string {
        return $this->planId;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param int $projectId
     */
    function setProjectId(int $projectId) {
        $this->projectId = $projectId;
    }

    /**
     * 
     * @param string $title
     */
    function setTitle(string $title) {
        $this->title = $title;
    }

    /**
     * 
     * @param string|null $description
     */
    function setDescription(?string $description) {
        $this->description = $description;
    }

    /**
     * 
     * @param int $amount
     */
    function setAmount(int $amount) {
        $this->amount = $amount;
    }

    /**
     * 
     * @param int|null $contributorsMax
     */
    function setContributorsMax(?int $contributorsMax) {
        $this->contributorsMax = $contributorsMax;
    }

    /**
     * 
     * @param int $contributorsNb
     */
    function setContributorsNb(int $contributorsNb) {
        $this->contributorsNb = $contributorsNb;
    }
    
    /**
     * 
     * @param string|null $productId
     */
    function setProductId(?string $productId) {
        $this->productId = $productId;
    }
    
    function setPlanId(string $planId) {
        $this->planId = $planId;
    }


}
