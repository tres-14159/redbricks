<?php

namespace App\Suggestion\Table;

use Framework\Database\Table;
use App\Suggestion\Entity\VotesSuggestion;
use Framework\Database\Query;

class VotesSuggestionTable extends Table {

    protected $entity = VotesSuggestion::class;
    protected $table = "votes_suggestion";

    /**
     * 
     * @param int $userId
     * @return Query
     */
    public function findByUser(int $userId): Query {
        return $this->makeQuery()
                        ->where("v.user_id = $userId");
    }

    /**
     * 
     * @param int $suggestionId
     * @param int $userId
     * @return bool
     */
    public function deleteBySuggest(int $suggestionId, int $userId): bool {
        $query = $this->pdo->prepare("DELETE FROM {$this->table} WHERE suggestion_id=:suggestion_id AND user_id=:user_id");
        $query->bindValue(':suggestion_id', $suggestionId);
        $query->bindValue(':user_id', $userId);
        return $query->execute();
    }

}
