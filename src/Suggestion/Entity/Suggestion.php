<?php

namespace App\Suggestion\Entity;

class Suggestion {

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $game;

    /**
     * @var string
     */
    protected $studio;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return string
     */
    function getGame(): string {
        return $this->game;
    }

    /**
     * 
     * @return string
     */
    function getStudio(): string {
        return $this->studio;
    }

    /**
     * 
     * @return \DateTime
     */
    function getCreatedAt(): \DateTime {
        return $this->createdAt;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param string $game
     */
    function setGame(string $game) {
        $this->game = $game;
    }

    /**
     * 
     * @param string $studio
     */
    function setStudio(string $studio) {
        $this->studio = $studio;
    }

    /**
     * @param \DateTime|string $datetime
     */
    function setCreatedAt($datetime) {
        if (is_string($datetime)) {
            $this->createdAt = new \DateTime($datetime);
        } else {
            $this->createdAt = $datetime;
        }
    }

}
