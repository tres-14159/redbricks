<?php

namespace App\Suggestion\Entity;

class VotesSuggestion {

    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $suggestionId;

    /**
     * @var int
     */
    protected $userId;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return int
     */
    function getSuggestionId(): int {
        return $this->suggestionId;
    }

    /**
     * 
     * @return int
     */
    function getUserId(): int {
        return $this->userId;
    }

    /**
     * 
     * @return \DateTime
     */
    function getCreatedAt(): \DateTime {
        return $this->createdAt;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param int $suggestionId
     */
    function setSuggestionId(int $suggestionId) {
        $this->suggestionId = $suggestionId;
    }

    /**
     * 
     * @param int $userId
     */
    function setUserId(int $userId) {
        $this->userId = $userId;
    }

    /**
     * @param \DateTime|string $datetime
     */
    function setCreatedAt($datetime) {
        if (is_string($datetime)) {
            $this->createdAt = new \DateTime($datetime);
        } else {
            $this->createdAt = $datetime;
        }
    }

}
