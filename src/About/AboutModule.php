<?php

namespace App\About;

use Framework\Module;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use App\About\Actions\AboutSecurityAction;
use App\About\Actions\AboutAction;
use App\About\Actions\AboutPrivacyAction;
use App\About\Actions\AboutTermsAction;
use App\About\Actions\BusinessModelAction;
use App\About\Actions\VisualIdentityAction;
use App\About\Actions\TeamAction;
use App\About\Actions\ValueAction;
use App\About\Actions\CharterAction;

class AboutModule extends Module {

    public function __construct(Router $router, RendererInterface $renderer) {
        $renderer->addPath('about', __DIR__ . '/views');
        $router->get('/about', AboutAction::class, 'about');
        $router->get('/about/security', AboutSecurityAction::class, 'about.security');
        $router->get('/about/privacy', AboutPrivacyAction::class, 'about.privacy');
        $router->get('/about/terms', AboutTermsAction::class, 'about.terms');
        $router->get('/about/team', TeamAction::class, 'about.team');
        $router->get('/about/value', ValueAction::class, 'about.value');
        $router->get('/about/visual-identity', VisualIdentityAction::class, 'about.visualIdentity');
        $router->get('/about/business-model', BusinessModelAction::class, 'about.businessModel');
        $router->get('/about/charter', CharterAction::class, 'about.charter');
    }

}
