<?php

namespace App\Bug;

use Framework\Module;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use App\Bug\Actions\BugShowAction;
use App\Bug\Actions\BugCrudAction;
use App\Bug\Actions\BugIndexAction;
use Framework\Auth\LoggedInMiddleware;
use App\Bug\Actions\BugListAction;

class BugModule extends Module {

    public function __construct(Router $router, RendererInterface $renderer) {
        $renderer->addPath('bug', __DIR__ .  "/views");
        $router->get('/bugs/index/{id:[0-9]+}', [LoggedInMiddleware::class, BugIndexAction::class], 'bugs');
        $router->crud("/bugs/{projectId:[0-9]+}", [LoggedInMiddleware::class, BugCrudAction::class], "bugs");
        $router->get('/bugs/bug/{id:[0-9]+}', BugShowAction::class, "bug");
        $router->get("/home/{slug:[a-z\-0-9]+}-{id:[0-9]+}/bugs", BugListAction::class, "bugs.list");
        $router->post("/home/{slug:[a-z\-0-9]+}-{id:[0-9]+}/bugs", [LoggedInMiddleware::class, BugListAction::class]);
    }

}
