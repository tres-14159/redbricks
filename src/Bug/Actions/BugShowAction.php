<?php

namespace App\Bug\Actions;

use App\Bug\Table\BugTable;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Auth;
use Psr\Http\Message\ServerRequestInterface;
use App\Blog\Table\AttachmentTable;
use App\Blog\Table\GameTable;
use App\Comments\Table\CommentsTable;

class BugShowAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var Auth
     */
    private $auth;
    
    /**
     *
     * @var BugTable
     */
    protected $bugTable;
    
    /**
     *
     * @var AttachmentTable
     */
    protected $attachmentTable;
    
    /**
     *
     * @var GameTable
     */
    protected $gameTable;
    
    /**
     *
     * @var CommentsTable
     */
    protected $commentsTable;

    public function __construct(
    RendererInterface $renderer, Router $router, Auth $auth, BugTable $bugTable, AttachmentTable $attachmentTable, GameTable $gameTable, CommentsTable $commentsTable) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->auth = $auth;
        $this->bugTable = $bugTable;
        $this->attachmentTable = $attachmentTable;
        $this->gameTable = $gameTable;
        $this->commentsTable = $commentsTable;
    }
    
    public function __invoke(ServerRequestInterface $request) {
        $bugId = $request->getAttribute("id");
        $bug = $this->bugTable->find($bugId);
        $attachments = $this->attachmentTable->findByBug($bugId);
        $game = $this->gameTable->find($bug->getProjectId());
        $comments = $this->commentsTable->findListWithChildrenByBug($bugId);
        return $this->renderer->render("@bug/show", compact("bug", "attachments", "game", "comments"));
    }

}
