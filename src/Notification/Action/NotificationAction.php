<?php

namespace App\Notification\Action;

use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Notification\Table\NotificationTable;
use Framework\Auth;
use Framework\Session\FlashService;

class NotificationAction {

    /**
     *
     * @var RendererInterface
     */
    protected $renderer;

    /**
     *
     * @var NotificationTable
     */
    protected $notificationTable;

    /**
     *
     * @var Auth
     */
    protected $auth;
    
    /**
     *
     * @var FlashService
     */
    protected $flash;
    
    protected $required_params = ["mp", "badge", "new_brick", "change_brick", "new_bug", "change_bug", "news", "tip", "deadline", "collect", "comment", "comment_contri", "change_contri"];

    public function __construct(
    RendererInterface $renderer, NotificationTable $notificationTable, Auth $auth, FlashService $flash
    ) {
        $this->renderer = $renderer;
        $this->notificationTable = $notificationTable;
        $this->auth = $auth;
        $this->flash = $flash;
    }

    public function __invoke(Request $request) {

        $user = $this->auth->getUser();
        if ($request->getMethod() === "POST") {
            $params = $request->getParsedBody();
            foreach($this->required_params as $key){
                $user_params[$key] = $params[$key] ?? '0';
            }
            $this->notificationTable->update($user->getId(), $user_params);
            $this->flash->success(_("Your notification preference have been saved."));
        }
        $notifications = $this->notificationTable->findBy("user_id", $user->getId());
        return $this->renderer->render("@notification/notification", compact('notifications'));
    }

}
