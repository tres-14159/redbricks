<?php

namespace App\Notification;

use Framework\Module;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Auth\LoggedInMiddleware;
use App\Notification\Action\NotificationAction;

class NotificationModule extends Module {
    
    const DEFINITIONS = __DIR__ . '/definitions.php';

    public function __construct(Router $router, RendererInterface $renderer) {
        $renderer->addPath('notification', __DIR__ . "/views");
        $router->get('/notification', [LoggedInMiddleware::class, NotificationAction::class], 'notification');
        $router->post('/notification', [LoggedInMiddleware::class, NotificationAction::class]);
    }

}
