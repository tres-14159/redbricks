<?php

namespace App\Support\Action;

use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Support\Table\ArticleTable;

class ArticleAction {

    /**
     *
     * @var RendererInterface
     */
    protected $renderer;

    /**
     *
     * @var ArticleTable
     */
    protected $articleTable;

    public function __construct(
    RendererInterface $renderer, ArticleTable $articleTable
    ) {
        $this->renderer = $renderer;
        $this->articleTable = $articleTable;
    }

    public function __invoke(Request $request) {

        $id = $request->getAttribute("id");
        $article = $this->articleTable->find($id);
        return $this->renderer->render("@support/article", compact('article'));
    }

}