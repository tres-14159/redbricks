<?php

namespace App\Account;

class User extends \App\Auth\User {

    /**
     * @var string
     */
    protected $displayname = "";

    /**
     * @var string
     */
    protected $avatar = "";

    /**
     * @var string
     */
    protected $website = "";

    /**
     * @var string
     */
    protected $profile = "";

    /**
     * @var string
     */
    protected $location = "";

    /**
     * @var string|null
     */
    protected $createdAt = "";

    public function getRoles(): array {
        return [$this->role];
    }

    /**
     * @return string
     */
    public function getDisplayname(): string {
        return $this->displayname;
    }

    /**
     * @param string $displayname
     */
    public function setDisplayname(string $displayname) {
        $this->displayname = $displayname;
    }

    /**
     * @return string|null
     */
    public function getAvatar(): ?string {
        return $this->avatar;
    }

    /**
     * @param string|null $avatar
     */
    public function setAvatar(?string $avatar) {
        $this->avatar = $avatar;
    }

    /**
     * 
     * @return string
     */
    public function getAvatarThumb(): string {
        ['filename' => $filename, 'extension' => $extension] = pathinfo($this->avatar);
        return '/uploads/avatars/' . $filename . '_thumb.' . $extension;
    }

    /**
     * 
     * @param null|string $avatar
     * @return string
     */
    public function setAvatarThumb(?string $avatar): string {
        $this->avatar = $avatar;
        return '/uploads/avatars/' . $this->avatar;
    }

    /**
     * 
     * @return string
     */
    public function getAvatarUrl(): string {
        return '/uploads/avatars/' . $this->avatar;
    }

    /**
     * @return string|null
     */
    public function getWebsite(): ?string {
        return $this->website;
    }

    /**
     * @param null|string $website
     */
    public function setWebsite(?string $website) {
        $this->website = $website;
    }

    /**
     * @return string|null
     */
    public function getProfile(): ?string {
        return $this->profile;
    }

    /**
     * @param null|string $profile
     */
    public function setProfile(?string $profile) {
        $this->profile = $profile;
    }

    /**
     * @return string|null
     */
    public function getLocation(): ?string {
        return $this->location;
    }

    /**
     * @param string|null $location
     */
    public function setLocation(?string $location) {
        $this->location = $location;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role) {
        $this->role = $role;
    }

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string {
        return $this->createdAt;
    }

    /**
     * 
     * @param string|null $createdAt
     */
    function setCreatedAt(?string $createdAt) {
        $this->createdAt = $createdAt;
    }

}
