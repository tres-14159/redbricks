<?php
return [
    'auth.entity' => \App\Account\User::class,
    'signup.from' => \DI\get('mail.from'),
    \App\Account\Action\SignupAction::class => \DI\object()->constructorParameter('from', \DI\get('signup.from'))
];
