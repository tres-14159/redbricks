<?php

namespace Framework\Api;

use Stripe\Card;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Token;
use Stripe\Account;
use Stripe\Plan;
use Stripe\Subscription;
use Stripe\Collection;
use Stripe\Product;
use Stripe\Checkout\Session;

class Stripe {

    public function __construct(string $token) {
        \Stripe\Stripe::setApiKey($token);
        \Stripe\Stripe::setApiVersion("2018-07-27");
    }

    public function getCardFromToken(string $token): Card {
        return Token::retrieve($token)->card;
    }

    public function getCard($cardId): Card {
        return Card::retrieve($cardId);
    }

    public function getCustomer($customerId): Customer {
        return Customer::retrieve($customerId);
    }

    public function getAccount($accountId): Account {
        return Account::retrieve($accountId);
    }
    
    public function getToken($tokenId): Token {
        return Token::retrieve($tokenId);
    }

    public function getPlan($planId, array $options = null): Plan {
        return Plan::retrieve($planId, $options);
    }

    public function getSubscription($subscriptionId, array $options = null): Subscription {
        return Subscription::retrieve($subscriptionId, $options);
    }
    
    public function getProduct(string $productId, array $options = null) {
        return Product::retrieve($productId, $options);
    }

    public function createAccount(array $params): Account {
        return Account::create($params);
    }

    public function createCustomer(array $params, array $options = null): Customer {
        return Customer::create($params, $options);
    }

    public function createCardForCustomer(Customer $customer, string $token): Card {
        return $customer->sources->create(['source' => $token]);
    }

    public function createCharge(array $params, array $options = null): Charge {
        return Charge::create($params, $options);
    }

    public function createToken(array $params, array $options = null): Token {
        return Token::create($params, $options);
    }

    public function createPlan(array $params, array $options = null): Plan {
        return Plan::create($params, $options);
    }

    public function createSubscription(array $params, array $options = null): Subscription {
        return Subscription::create($params, $options);
    }
    
    public function createProduct(array $params, array $options = null): Product {
        return Product::create($params, $options);
    }
    
    public function createCheckoutSession(array $params, array $options = null): Session {
        return Session::create($params, $options);
    }
    
    public function allPlan(array $params = null, array $options = null): Collection {
        return Plan::all($params, $options);
    }
    
    public function allSubscriptions(array $params = null, array $options = null): Collection {
        return Subscription::all($params, $options);
    }
    
    public function allProduct(array $params = null, array $options = null): Collection {
        return Product::all($params, $options);
    }
    
    public function cancelSubscription(string $subscriptionId, array $options = null): Subscription {
        $subscription = $this->getSubscription($subscriptionId, $options);
        return $subscription->cancel();
    }
    
    public function updateProduct(string $productId, array $params = null, array $options = null): Product {
        return Product::update($productId, $params, $options);
    }
    
    public function updatePlan(string $planId, array $params = null, array $options = null): Plan {
        return Plan::update($planId, $params, $options);
    }

}
