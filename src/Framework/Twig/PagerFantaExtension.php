<?php

namespace Framework\Twig;

use Pagerfanta\View\TwitterBootstrap4View;
use Pagerfanta\Pagerfanta;
use Framework\Router;

class PagerFantaExtension extends \Twig_Extension {

    /**
     * @var Router
     */
    private $router;

    public function __construct(Router $router) {
        $this->router = $router;
    }

    public function getFunctions() {

        return [
            new \Twig_SimpleFunction("paginate", [$this, "paginate"], ["is_safe" => ["html"]])
        ];
    }

    /**
     * Génère la pagination
     */
    public function paginate(
    Pagerfanta $paginatedResults, string $route, array $routerParams = [], array $queryArgs = []
    ): string {
        //var_dump($routerParams); 				
        //var_dump( $this->router->generateUri("blog.licence","free-game", $queryArgs));die();		
        $view = new TwitterBootstrap4View();
        return $view->render($paginatedResults, function (int $page) use ($route, $routerParams, $queryArgs) {
                    if ($page > 1) {
                        $queryArgs["p"] = $page;
                    }
                    return $this->router->generateUri($route, $routerParams, $queryArgs);
                });
    }

}
