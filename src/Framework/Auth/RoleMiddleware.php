<?php
namespace Framework\Auth;

use Framework\Auth;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class RoleMiddleware implements MiddlewareInterface
{

    /**
     * @var Auth
     */
    private $auth;
    /**
     * @var array
     */
    private $role;
    
    private $is_valid;

    public function __construct(Auth $auth, array $role)
    {
        $this->auth = $auth;
        $this->role = $role;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate): ResponseInterface
    {
        $user = $this->auth->getUser();

        $is_valid= false;
        if ($user !== null) {
            foreach ($this->role as $key) {
                if (in_array($key, $user->getRoles()) or $user->getRoles()== ["admin"]) {
                    $is_valid= true;
                    break;
                }
            }
        }

        if ($user === null || !$is_valid) {
            throw new ForbiddenException();
        }
        return $delegate->process($request);
    }
}
