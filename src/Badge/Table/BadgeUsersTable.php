<?php

namespace App\Badge\Table;

use Framework\Database\Table;
use App\Badge\Entity\BadgeUsers;
use App\Badge\Table\BadgeTable;
use Framework\Database\Query;

class BadgeUsersTable extends Table {

    protected $table = "badge_users";
    protected $entity = BadgeUsers::class;

    /**
     * 
     * @return Query
     */
    public function findAll(): Query {
        $badge = new BadgeTable($this->pdo);
        $name = "badge.name_" . getenv("LANG");
        $description = "badge.description_" . getenv("LANG");
        return $this->makeQuery()
                        ->select("b.*, $name as name, $description as description")
                        ->join($badge->getTable() . ' as badge', "badge.id=b.badge_id");
    }

    /**
     * 
     * @param int $userId
     * @return Query
     */
    public function findForUser(int $userId): Query {
        return $this->findAll()
                        ->where("b.user_id=$userId")
                        ->order("b.badge_id");
    }

}
