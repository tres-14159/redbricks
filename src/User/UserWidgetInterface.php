<?php
namespace App\User;

interface UserWidgetInterface
{
    
//  public function getPosition(): int;//A développer si on veut régler la position des widgets
    
    public function render(): string;

    public function renderMenu(): string;
}
