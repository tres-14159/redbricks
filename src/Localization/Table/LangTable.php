<?php

namespace App\Localization\Table;

use Framework\Database\Table;
use App\Localization\Entity\Lang;
use Framework\Database\Query;
use App\Blog\Table\FeatureContentTable;

class LangTable extends Table {

    protected $entity = Lang::class;
    protected $table = "lang";

    public function unusedLanguage(int $gameId): Query {
        return $this->makeQuery()
                        ->select("l.*")
                        ->where("l.code NOT IN (SELECT langcode FROM games_content as g WHERE g.game_id = $gameId)");
    }

    public function unusedLanguageForFeature(int $featureId): Query {
        return $this->makeQuery()
                        ->select("l.*")
                        ->where("l.code NOT IN (SELECT langcode FROM features_content as f WHERE f.feature_id = $featureId)");
    }

}
