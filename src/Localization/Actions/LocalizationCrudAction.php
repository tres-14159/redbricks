<?php

namespace App\Localization\Actions;

use Framework\Actions\CrudAction;
use Framework\Session\FlashService;
use Framework\Router;
use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Blog\Table\GameContentTable;
use App\Localization\Table\LangTable;
use App\Blog\Table\GameTable;
use App\Blog\Table\ContributorTable;
use Framework\Session\SessionInterface;
use Framework\Auth;

class LocalizationCrudAction extends CrudAction {

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var LangTable
     */
    protected $langTable;

    /**
     *
     * @var ContributorTable
     */
    protected $contributorTable;

    /**
     *
     * @var SessionInterface
     */
    protected $session;
    protected $acceptedParams = ["game_id", "content", "langcode"];

    public function __construct(
            RendererInterface $renderer, Router $router, GameContentTable $table, FlashService $flash, LangTable $langTable, GameTable $gameTable, ContributorTable $contributorTable, SessionInterface $session) {
        parent::__construct($renderer, $router, $table, $flash);
        $this->langTable = $langTable;
        $this->gameTable = $gameTable;
        $this->contributorTable = $contributorTable;
        $this->session = $session;
        $this->messages = [
            "create" => _("A translation has been created."),
            "edit" => _("The translation has been modified.")
        ];
    }

    public function edit(ServerRequestInterface $request) {
        $item = $this->table->find($request->getAttribute("id"));
        $game = $this->gameTable->find($item->getGameId());
        $this->validateAccessRights($game->getUserId(), $game->getId());
        $languages = $this->langTable->findAll();
        if ($request->getMethod() === "POST") {
            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                $fields = $this->prePersist($request, $item);
                //echo '<pre>'; var_dump($item, $fields); echo '</pre>'; die();
                $this->table->update($item->getId(), $fields);
                $this->flash->success($this->messages["edit"]);
                $this->postPersist($request, $item);
                return $this->redirect("localization.translation", [
                            "id" => $item->getGameId()
                ]);
            }
            $errors = $validator->getErrors();
            Hydrator::hydrate($request->getParsedBody(), $item);
        }
        return $this->renderer->render('@localization/edit', $this->formParams(compact("item", "errors", "languages")));
    }

    public function create(ServerRequestInterface $request) {
        $item = $this->getNewEntity();
        $item->setGameId($request->getAttribute('projectId'));
        $game = $this->gameTable->find($item->getGameId());
        $this->validateAccessRights($game->getUserId(), $game->getId());
        $item->title = $game->getName();
        $item->setContent($game->getContent());
        $languages = $this->langTable->unusedLanguage($item->getGameId());
        if ($request->getMethod() === "POST") {
            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                $this->table->insert($this->prePersist($request, $item));
                $this->flash->success($this->messages["create"]);
                $this->postPersist($request, $item);
                return $this->redirect("localization.translation", [
                            "id" => $item->getGameId()
                ]);
            }
            $errors = $validator->getErrors();
            Hydrator::hydrate($request->getParsedBody(), $item);
        }
        return $this->renderer->render('@localization/create', $this->formParams(compact("item", "errors", "languages")));
    }

    public function prePersist(ServerRequestInterface $request, $item): array {
        $params = $request->getParsedBody();
        if (!empty($request->getAttribute("projectId"))) {
            $params["game_id"] = $request->getAttribute("projectId");
        }

        //echo '<pre>';        var_dump($params, $this->acceptedParams); echo '</pre>'; die();
        return array_filter($params, function ($key) {
            return in_array($key, $this->acceptedParams);
        }, ARRAY_FILTER_USE_KEY);
    }

    public function delete(ServerRequestInterface $request) {
        $game = $this->gameTable->find($request->getAttribute("projectId"));
        $this->validateAccessRights($game->getUserId(), $game->getId());
        $this->table->delete($request->getAttribute("id"));
        return $this->redirect("localization.translation", [
                    "id" => $game->getId()
        ]);
    }

    protected function validateAccessRights(int $itemUserId, int $gameId) {
        $role = $this->session->get('auth.role');
        $userId = $this->session->get('auth.user');
        if ($role == null || ($role == 'user' && $userId != $itemUserId && !$this->contributorTable->isContributor($gameId, $userId))) {
            throw new Auth\ForbiddenException();
        }
    }

}
