<?php

use App\Account\AccountModule;
use App\Admin\AdminModule;
use App\Discussion\DiscussionModule;
use App\Profile\ProfileModule;
use App\User\UserModule;
use App\Blog\BlogModule;
use App\Auth\AuthModule;
use App\Contact\ContactModule;
use App\Shop\ShopModule;
use App\Start\StartModule;
use App\About\AboutModule;
use App\Bug\BugModule;
use App\Comments\CommentsModule;
use App\News\NewsModule;
use App\Support\SupportModule;
use App\Notification\NotificationModule;
use App\Badge\BadgeModule;
use App\Survey\SurveyModule;
use App\Suggestion\SuggestionModule;
use App\Localization\LocalizationModule;
use Framework\Auth\RoleMiddlewareFactory;
use Framework\Middleware\CsrfMiddleware;
use Framework\Middleware\DispatcherMiddleware;
use Framework\Middleware\MethodMiddleware;
use Framework\Middleware\RouterMiddleware;
use Framework\Middleware\TrailingSlashMiddleware;
use Framework\Middleware\NotFoundMiddleware;
use Framework\Middleware\RendererRequestMiddleware;
use Framework\Middleware\LocalizationMiddleware;
use GuzzleHttp\Psr7\ServerRequest;
use Middlewares\Whoops;

chdir(dirname(__DIR__));

require "vendor/autoload.php";

$app = (new \Framework\App(['config/config.php', 'config.php']))
        ->addModule(AdminModule::class)
        ->addModule(UserModule::class)
        ->addModule(ContactModule::class)
        ->addModule(AboutModule::class)
        ->addModule(ShopModule::class)
        ->addModule(BlogModule::class)
        ->addModule(AuthModule::class)
        ->addModule(AccountModule::class)
        ->addModule(StartModule::class)
        ->addModule(ProfileModule::class)
        ->addModule(DiscussionModule::class)
        ->addModule(BugModule::class)
        ->addModule(CommentsModule::class)
        ->addModule(NewsModule::class)
        ->addModule(SupportModule::class)
        ->addModule(NotificationModule::class)
        ->addModule(BadgeModule::class)
        ->addModule(SurveyModule::class)
        ->addModule(SuggestionModule::class)
        ->addModule(LocalizationModule::class);

$container = $app->getContainer();
//Toujours placer NotFoundMiddleware à la fin
//  Essai fait avec dans RoleMiddlewareFactory: ['admin', 'user'])
$container->get(\Framework\Router::class)->get('/', \App\Blog\Actions\GameIndexAction::class, 'home');
$app->pipe(Whoops::class)
        ->pipe(LocalizationMiddleware::class)
        ->pipe(TrailingSlashMiddleware::class)
        ->pipe(\App\Auth\ForbiddenMiddleware::class)
        ->pipe(
                $container->get('admin.prefix'), $container->get(RoleMiddlewareFactory::class)->makeForRole(['admin'])
        )
        ->pipe(
                $container->get('user.prefix'), $container->get(RoleMiddlewareFactory::class)->makeForRole(['user'])
        )
        ->pipe(MethodMiddleware::class)
        ->pipe(RendererRequestMiddleware::class)
        ->pipe(CsrfMiddleware::class)
        ->pipe(RouterMiddleware::class)
        ->pipe(DispatcherMiddleware::class)
        ->pipe(NotFoundMiddleware::class);

//php_sapi_name() retourne le type d'interface utilisé (cli est la ligne de commande)
if (php_sapi_name() !== "cli") {
    $response = $app->run(ServerRequest::fromGlobals());
    \Http\response\send($response);
}
